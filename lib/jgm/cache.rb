module Jgm
  class Cache

    attr_accessor :prefix
    attr_reader :store

    def initialize
      @prefix = 'jgm'
    end

    def store=(store)
      @store = Jgm::StoreProxy.build(store)
    end

    def count(key, period)
      unix = Time.now.to_i
      expires_in = period - (unix % period)
      key = "#{prefix}:#{key}:#{(unix/period).to_i}"
      value = store.increment(key, 1, :expires_in => expires_in)
      puts "#{Time.now}:#{key}:#{value}"
      [value, store.ttl(key)]
    end

    def read(key)
      store.read("#{prefix}:#{key}")
    end

    def write(key, value, expires_in)
      store.write("#{prefix}:#{key}", value, :expires_in => expires_in)
    end

  end
end