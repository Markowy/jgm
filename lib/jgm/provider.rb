module Jgm
  class ProviderLimitExceeded < StandardError; end

  class Provider

    attr_reader :name, :limit, :period_in_sec, :cache_key

    def initialize(name, opts)
      @name = name
      @limit = opts['limit']
      @period_in_sec = opts['period_in_sec']
      @cache_key = @name
    end

    def perform_call
      current_value, ttl = Jgm.cache.count(cache_key, period_in_sec)
      if current_value < limit
        call
      else
        raise ProviderLimitExceeded, "Limit exceeded for: #{name}. Next attempt in #{ttl} sec"
      end
    rescue ProviderLimitExceeded => e
      puts e.message
    end

    private

    def call
      # call to fb, twitter etc...
    end

  end
end