require 'delegate'

module Jgm
  class MemcachedProxy < SimpleDelegator
  	
    def initialize(store)
      raise 'not implemented'
    end

    def read(key)
    	raise 'not implemented'
    end

    def write(key, value, options={})
    	raise 'not implemented'    	
    end

    def increment(key, amount, options={})
    	raise 'not implemented'    	
    end

  end
end