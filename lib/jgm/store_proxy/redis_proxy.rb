require 'delegate'

module Jgm
  class RedisProxy < SimpleDelegator

    def initialize(store)
      super(store)
    end

    def read(key)
      self.get(key)
    end

    def write(key, value, options={})
      if (expires_in = options[:expires_in])
        self.setex(key, expires_in, value)
      else
        self.set(key, value)
      end
    end

    def increment(key, amount, options={})
      count = nil
      self.pipelined do
        count = self.incrby(key, amount)
        self.expire(key, options[:expires_in]) if options[:expires_in]
      end
      count.value if count
    end

    def increment(key, amount, options={})
      count = nil
      self.pipelined do
        count = self.incrby(key, amount)
        self.expire(key, options[:expires_in]) if options[:expires_in]
      end
      count.value if count
    end
    
  end

end