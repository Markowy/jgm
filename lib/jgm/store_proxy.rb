module Jgm
  class StoreProxy
    def self.build(store)
    	if defined?(Redis) && store.is_a?(Redis)
    		Jgm::RedisProxy.new(store)
    	elsif defined?(Memcached) && store.is_a?(Memcached)
				Jgm::MemcachedProxy.new(store)
			else
				store
			end
		end
 	end
end