require "jgm/version"
require "jgm/provider"
require "jgm/cache"
require "jgm/store_proxy"
require "jgm/store_proxy/redis_proxy"
require "jgm/store_proxy/memcached_proxy"
require "yaml"
require "redis"

module Jgm

  class << self

    CONFIG = YAML.load_file(File.join(__dir__, '../config/config.yml'))

    def cache
      @cache ||= Cache.new
    end

    def providers
      @providers ||= load_providers
    end

    def load_providers
      CONFIG['api_providers'].map{|provider| Jgm::Provider.new(*provider)}
    end

  end

end