require 'jgm'

threads = []
5.times { |i|
	threads << Thread.new {
		Jgm.cache.store = Redis.new
		loop do
		  Jgm.providers[rand(0..2)].perform_call
		  sleep(rand(0.5..1.0))
		end
	}
}
threads.each { |t| t.join }